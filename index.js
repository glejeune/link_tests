const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const querystring = require('querystring');
const port = process.argv[2] || 5000;

const MIMETYPES = {
  '.ico': 'image/x-icon',
  '.html': 'text/html',
  '.js': 'text/javascript',
  '.json': 'application/json',
  '.css': 'text/css',
  '.png': 'image/png',
  '.jpg': 'image/jpeg',
  '.wav': 'audio/wav',
  '.mp3': 'audio/mpeg',
  '.svg': 'image/svg+xml',
  '.pdf': 'application/pdf',
  '.doc': 'application/msword',
};
const META = /<!-- meta -->/;
const QUERY_STRING = /<!-- qs:(.) -->/g;

const LINKS = {
  main_css: {
    url: '/assets/main.css',
    as: 'style',
    prefix: '?',
  },
  main_js: {
    url: '/assets/main.js',
    as: 'script',
    prefix: '?',
  },
  spinner_gif: {
    url: '/assets/spinner.gif',
    as: 'image',
    prefix: '?',
  },
  jquery_js: {
    url: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js',
    as: 'script',
    prefix: '?',
  },
  anime_js: {
    url: 'https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.js',
    as: 'script',
    prefix: '?',
  },
  showdown_js: {
    url: 'https://cdnjs.cloudflare.com/ajax/libs/showdown/1.9.0/showdown.min.js',
    as: 'script',
    prefix: '?',
  },
  poppins_css: {
    url: 'https://fonts.googleapis.com/css?family=Poppins:600',
    as: 'style',
    prefix: '&',
  },
  poppins_woff2_1: {
    url:
      'https://fonts.gstatic.com/s/poppins/v5/pxiByp8kv8JHgFVrLEj6Z11lFc-K.woff2',
    as: 'font',
    prefix: undefined,
  },
  poppins_woff2_2: {
    url:
      'https://fonts.gstatic.com/s/poppins/v5/pxiByp8kv8JHgFVrLEj6Z1xlFQ.woff2',
    as: 'font',
    prefix: undefined,
  },
  readme_md: {
    url: '/README.md',
    as: 'fetch',
    prefix: undefined,
  }
};

function sendData(res, wait, data, start, len) {
  setTimeout(() => {
    console.log(`${Date.now()}: ${start} -> ${start + len}`);
    res.write(data.slice(start, start + len));
    if (start + len < data.length) {
      sendData(res, wait, data, start + len, len);
    } else {
      res.end();
    }
  }, wait);
}

function getLinks(query) {
  return query.split(',')
    .map(k => k.trim())
    .reduce((acc, k) => {
      if (k === 'all') {
        acc = LINKS;
      } else {
        if (!acc[k] && LINKS[k]) {
          acc[k] = LINKS[k];
        }
      }
      return acc;
    }, {});
}

function render_static(pathname, qs, res) {
  fs.stat(pathname, (err, stats) => {
    if (err) {
      switch (err.code) {
        case 'ENOENT':
          res.statusCode = 404;
          break;
        default:
          res.statusCode = 500;
          break;
      }
      res.end();
      return;
    }

    if (stats.isDirectory()) {
      render_static(path.join(pathname, 'index.html'), qs, res);
      return;
    }

    const ext = path.parse(pathname).ext;

    let headers = {
      connection: 'keep-alive',
      'Content-type': MIMETYPES[ext] || 'text/plain',
      'Transfer-Encoding': 'chunked',
      'Cache-Control': 'private, no-cache',
    };

    const query_string = qs['version'] && qs['version'] === 'true' ? `v=${Date.now()}` : undefined;

    const preload_headers = Object.values(
      qs['preload_headers'] && qs['preload_headers'].trim().length > 0
        ? getLinks(qs['preload_headers'])
        : {},
    ).map(({url, as, prefix}) => {
      let finalUrl = url;
      if (prefix && query_string) {
        finalUrl += `${prefix}${query_string}`
      }
      let data = `<${finalUrl}>; rel=preload; as=${as}`
      if (qs['cors']) {
        data = `${data}; crossorigin=${qs['cors']}`
      }
      return data;
    });
    if (preload_headers.length > 0) {
      headers = {...headers, Link: preload_headers};
    }

    const preload_html = Object.values(
      qs['preload_html'] && qs['preload_html'].trim().length > 0
        ? getLinks(qs['preload_html'])
        : {},
    ).map(({url, as, prefix}) => {
      let finalUrl = url;
      if (prefix && query_string) {
        finalUrl += `${prefix}${query_string}`
      }
      let data = `<link href="${finalUrl}" rel="preload" as="${as}"`
      if (qs['cors']) {
        data = `${data} crossorigin="${qs['cors']}"`
      }
      data = `${data}>`
      return data;
    }).join('\n    ');

    fs.readFile(pathname, (err, data) => {
      if (err) {
        res.statusCode = 500;
        res.end();
      } else {

        if (META.test(data.toString('utf-8'))) {
          data = Buffer.from(data.toString().replace(META, preload_html));
        }

        if (QUERY_STRING.test(data.toString('utf-8'))) {
          if (query_string) {
            data = Buffer.from(data.toString().replace(QUERY_STRING, `$1${query_string}`));
          } else {
            data = Buffer.from(data.toString().replace(QUERY_STRING, ''));
          }
        }

        res.writeHead(200, headers);
        let wait = qs['wait'] || 0;
        let packet = Number(qs['packet'] || data.length);

        // That's ugly DUDE !
        if (qs['headers'] === 'true') {
          res._finished = true;
          res.socket.write(res._header);
          res._headerSent = true;
        }
        if (qs['zbb'] === 'true') {
          res.write(' ')
        }

        packet = packet < 1 ? 1 : packet;
        packet = packet > data.length ? data.length : packet;
        wait = Math.ceil(wait / (data.length / packet));

        sendData(res, wait, data, 0, packet);
      }
    });
  });
}

http
  .createServer((req, res) => {

    const parsedUrl = url.parse(req.url);
    const pathname = path.join('.', 'static', parsedUrl.pathname);
    const qs = querystring.parse(parsedUrl.query);

    console.log(`${req.method} ${req.url} [${pathname}]`);

    render_static(pathname, qs, res);
  })
  .listen(parseInt(port));

console.log(`Server listening on port ${port}`);
