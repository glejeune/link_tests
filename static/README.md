## About

The aim of this project is to test the comportment of a browser when resources are:

* Not preloaded.
* Preloaded via a [Link HTTP header](https://tools.ietf.org/html/rfc5988#section-5).
* Preloaded via a [link](https://developer.mozilla.org/en-US/docs/Web/HTML/Preloading_content) in the HTML head.

## Resources

This page load the following resources :

* [main_css]: The main CSS, from this server
* [main_js]: the main JS, from this server
* [spinner_gif]: The spinner image, from this server
* [jquery_js]: The jQuery lib from cdnjs.cloudflare.com
* [anime_js]: The anime.js lib from cdnjs.cloudflare.com
* [showdown_js]: The showdown.js lib from cdnjs.cloudflare.com
* [poppins_css]: The Poppins CSS, from fonts.googleapis.com

The poppins CSS load the following resources:

* [poppins_woff2_1]: A Poppins WOFF2 font
* [poppins_woff2_2]: A Poppins WOFF2 font

## Usage

You can play with le following parameters :

* `wait :: Integer`: Delay in ms between each body packets send (default: 0).
* `packet :: Integer`: The size of each body packets (default: body length).
* `headers :: Boolean`: If true, the headers will be send as soon as they are availables (default: false).
* `preload_html :: String`: List of resources (see bellow) to preload via a link in the HTML header (can be `all`) (default: none).
* `preload_headers :: String`: List of resources (see bellow) to preload via a Link HTTP header (can be `all`) (default: none).
* `zbb :: Boolean`: If true, send a zero byte body before waiting to send the first body packet (default: false).
* `cors :: String`: Add a `crossorigin` parameter to the link preload (can be `anonymous` or `use-credentials`) (default: none) \[[more](https://developer.mozilla.org/en-US/docs/Web/HTML/Preloading_content#Cross-origin_fetches)\].
* `version :: Boolean`: If true, we will add a random query parameter to each resources (except the Poppins fonts) (default: false).

## Configuration

### NginX

If you place this project behind a NginX server, do not forget to add 

```
proxy_buffering off;
proxy_request_buffering off;
```

in your `location` block.

### Firefox

If you want to test with firefox, you must set `network.preload` to `true` in the [configuration](https://support.mozilla.org/en-US/kb/about-config-editor-firefox).
